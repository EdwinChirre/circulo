from django.apps import AppConfig


class QueryMakerConfig(AppConfig):
    name = 'query_maker'
