import os
import datetime
from dotenv import load_dotenv

import time

import django
from django.apps import apps

import csv

from query_modules.helper import get_emission_date_str
from query import query


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "telcoCirculo.settings")
django.setup()

load_dotenv()

CIRCULO_API_KEY = os.getenv('CIRCULO_API_KEY')
CIRCULO_USERNAME = os.getenv('CIRCULO_USERNAME')
CIRCULO_PASSWORD = os.getenv('CIRCULO_PASSWORD')

Consulta = apps.get_model('query_maker','Consulta')

emission_date = datetime.datetime.now()
emission_date_str = get_emission_date_str(emission_date)

input_filedir = 'input'

RUCs_filename = 'RUCs.csv'
RUCs_filepath = input_filedir + '/' + RUCs_filename

fields = ['RUC']

RUCs = []

with open(RUCs_filepath, newline='') as csvfile:
    rows = csv.reader(csvfile, delimiter=',')
    next(rows, None)  # skip the header
    for row in rows:
        RUCs.append(row[0])

print(f'len(RUCs): {len(RUCs)}')

limit = 100

start = time.process_time()

for key, RUC in enumerate(RUCs):
    print(f'key: {key}. RUC: {RUC}')
    query(RUC, Consulta, CIRCULO_API_KEY, CIRCULO_USERNAME, CIRCULO_PASSWORD, key+1, emission_date_str)
    limit = limit - 1
    if limit == 0:
        break

elapsed_time = time.process_time() - start

print(f'elapsed time: {elapsed_time}')
