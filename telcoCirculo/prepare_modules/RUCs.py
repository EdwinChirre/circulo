import pandas as pd


def prepare_RUCs():

    ofilename = 'telco.csv'
    filedir = 'input'
    ofilepath = filedir + '/' + ofilename

    fields = ['RUC']

    df_RUC = pd.read_csv(ofilepath, skipinitialspace=True, usecols=fields)

    filename = 'RUCs.csv'
    filepath = filedir + '/' + filename

    df_RUC.to_csv(filepath, index=False)
