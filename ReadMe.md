telco - Circulo
===================================


Consulta a Circulo API para telco


Basic Commands
--------------

 
with venv activated:

```
pip install -r requirements.txt
cd telcoCirculo/
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

```
python consulta.py
```

Importación de datos
--------------------

Verificar estructura de base de datos
```
python manage.py makemigrations
python manage.py migrate
```

Ejecutar importación

```
python importa.py
```
 